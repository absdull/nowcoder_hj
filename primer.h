#pragma once
#include<string>
#include<vector>
#include<map>
//#include<Afxsock.h>
#include<windows.h>
using namespace std;

class Network {
public:
    bool send();
    static Network* New();
    static void Delete(Network* network);
protected:
    Network() {};
    ~Network() {};
};

class NetworkImpl :public Network {
    friend class Network;
private:
    string host;
    uint16_t port;
};

//用户配置组件
class DynamicPolicy {
    string flight_id;
    pair<double, double> location;//所在经纬度坐标
    double fuel;//油量
};
class Aircraft {
    string id;//所配置飞行器的ID，要唯一
    string ARN;//飞行器注册码（机尾号）
    string airline_code;//所属航空公司代码
    DynamicPolicy dynamicInfo;//飞行器动态信息及其变化策略
};

class TerminalUser {
    string id;//所配置的终端ID，要唯一
    string code;//终端代码
};
class RGS {
    string id;//所配置的RGS ID，要唯一
    string code;//RGS代码
    pair<double, double> location;//RGS所在经纬度坐标
};
class NMDPS {
    string id;//所配置的NMDPS ID，要唯一
    string code;//NMDPS代码
    string DSP_code;//所属DSP代码
};
class UserCongfig {
    map<string, string> routeTable;//路由表配置
    vector<Aircraft> aircrafts;//已配置的所有飞行器
    vector<TerminalUser> terminalUsers;//已配置的所有终端用户
    vector<RGS> RGSs;//已配置的所有RGS
    vector<NMDPS> NMDPSs;//已配置的所有NMDPS
public:
    UserCongfig() = default;
    UserCongfig(string config_file);//从配置文件初始化UserConfig组件
public:
    void LoadConfig(string config_file);//加载配置文件，给相关成员重新赋值
    void WriteConfig(string config_file);//将当前配置写入配置文件
public:
    string getRoute(string destination);//根据目的地址得到相应的路由转发IP
    Aircraft getAircraft(string id);//根据飞行器ID得到相应的飞行器结构体
    TerminalUser getTerminalUser(string id);//根据终端用户ID得到相应的终端用户结构体
    RGS getRGS(string id);//根据RGS ID得到相应的RGS结构体
    NMDPS getNMDPS(string id);//根据NMDPS ID得到相应的NMDPS结构体
};

//网络收发组件
//class NetworkUtility:CAsyncSocket {
//    UINT data_length;
//    byte data_buffer[4096];
//public:
//    NetworkUtility(string ip, int port);
//public:
//    void SendData(byte* send_buffer, string dest_ip, int dest_port);
//    void OnReceive(int errorcode);
//};

//报文管理组件
enum MessageType {
    UNKNOWN = -1,
    ARINC618 = 0,
    ARINC620 = 1
};

enum MessageDirection {
    Up = 0,
    Down=1
};

class ArincMessage {
public:
    MessageType messageType;
    MessageDirection messageDirection;
    bool isReceived;//标志报文是发送出去的还是接收到的
    string raw_text;
    string free_text;
};

class Arinc618Text {
public:
    char LABEL[2];
};

class Arinc618Text_QE :public Arinc618Text {
public:
    char message;
    char sequence[2];
    char number;
    char flight_number[6];
    char departure_station[3];
    char out_time_hours[2];
    char out_time_minites[2];
    char boarded_fuel[5];
    char fuel_quantity[4];
    char destination_station[3];
};

class Arinc618Message:public ArincMessage {
public:
    char mode;
    char ARN[7];//飞行器注册码，即机尾号，最长7位，不足时'.'补齐
    char TAK;
    char LABEL[2];//消息类型label，决定Arinc618Text的类型
    char DBI;
    char UBI;
    Arinc618Text text;
    bool isComplete;//报文是否组包完成
};

class Arinc620Text {
public:
    char FI[6];
    char AN[7];//机尾号最长7位
    char MA[4];//报文发送确认
};

class Arinc620Message :public ArincMessage {
public:
    char priority[2];
    char dest_addr[7];
    char transfer_time[4];
    char SMI[3];
    Arinc620Text text;
};

class MessageManager {
private:
    vector<ArincMessage*> arinc_messages{};//报文队列
public:
    void write2db(ArincMessage& m);//写入数据库
    void write2file(vector<ArincMessage*> selected_message_list);//本地存档
    void display(ArincMessage& m);//监控界面显示
};

//协议处理组件
class ProtocolProcessor {
private:
    void decode618(ArincMessage* arinc_message);//解析618格式
    void decode620(ArincMessage* arinc_message);//解析620格式
public:
    ArincMessage* getMessageType(byte* package);//判断报文类别,方向，BCS校检
    ArincMessage* decodePackage(byte* package);//解析报文得到结构体
    byte* convertMessage(byte* package, NMDPS *nmdps);//Arinc618与Arinc620协议之间转换
    byte* buildMessage(ArincMessage *arinc_message);//由报文结构体构造出报文
};

void ProtocolProcessor::decode618(ArincMessage* arinc_message) {
    string text = arinc_message->raw_text;
    if (text[0] != 1) {
        arinc_message = nullptr;
        return;
    }
    if (arinc_message->messageDirection == Down) {
        Arinc618Message* arinc_618_message = (Arinc618Message*)arinc_message;
        arinc_618_message->mode = text[1];
        strcpy_s<7>(arinc_618_message->ARN,text.substr(2, 7).c_str());
        arinc_618_message->TAK = text[9];
        strcpy_s<2>(arinc_618_message->LABEL, text.substr(10, 2).c_str());
        arinc_618_message->DBI=text[12];
        if (text[13] != 2) {
            arinc_message = nullptr;
            return;
        }

    }
    else {

    }

}

ArincMessage* ProtocolProcessor::decodePackage(byte* package) {
    ArincMessage* result = getMessageType(package);
    switch (result->messageType)
    {
    case UNKNOWN:
        break;
    case ARINC618:
        decode618(result);
        break;
    case ARINC620:
        decode620(result);
        break;
    default:
        break;
    }
    return result;
}

