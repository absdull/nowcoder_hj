﻿#include<iostream>
#include<string>
#include<fstream>
#include<map>
#include<memory>
#include<future>
#include<functional>
#include<thread>
#include<unordered_set>
#include<Windows.h>

#include<cassert>

#include<tuple>
#include<queue>

//#include "primer.h"

using namespace std;

#define REDIRECT_CIN \
    ifstream infile("data.txt"); \
    assert(infile.is_open()); \
    streambuf* oldbuf = cin.rdbuf(); \
    cin.rdbuf(infile.rdbuf());

double get_value(vector<double> nums, vector<int> ops) {
    int len = nums.size();
    if (len == 1)
        return nums[0];
    int prior_op_index = -1;
    for (int i = 0; i < ops.size(); ++i) {
        if (ops[i] == 2 || ops[i] == 3) {
            prior_op_index = i;
            break;
        }
    }
    vector<double> new_vec{};
    vector<int> new_op = ops;
    if (prior_op_index != -1) {
        for (int k = 0; k < len; ++k) {
            if (k != prior_op_index && k != prior_op_index + 1)
                new_vec.push_back(nums[k]);
        }
    }
    else {
        for (int k = 0; k < len; ++k) {
            if (k != 0 && k != 1)
                new_vec.push_back(nums[k]);
        }
    }
    double temp;
    if (prior_op_index == -1) {//0,1没有乘除
        if (ops[0] == 0)//+
            temp = nums[0] + nums[1];
        else //-
            temp = nums[0] - nums[1];
        new_vec.insert(new_vec.cbegin(), temp);
        new_op.erase(new_op.begin());
        return get_value(new_vec, new_op);
    }
    else {
        if (ops[prior_op_index] == 2)//*
            temp = nums[prior_op_index] * nums[prior_op_index + 1];
        else // /
            temp = nums[prior_op_index] / nums[prior_op_index + 1];
        new_vec.insert(new_vec.cbegin() + prior_op_index, temp);
        auto it = new_op.begin() + prior_op_index;
        new_op.erase(it);
        return get_value(new_vec, new_op);
    }
}

char get_op(int op) {
    switch (op)
    {
    case 0:
        return '+';
    case 1:
        return '-';
        break;
    case 2:
        return '*';
        break;
    case 3:
        return '/';
        break;
    default:
        return '\0';
    }
}

void twentyfour_Points(vector<double> nums) {
    vector<int> ops{ 0,1,2,3 };
    double first, second, third, fourth;
    int op1, op2, op3;
    for (int m = 0; m < 4; ++m) {
        first = nums[m];
        for (int n = 0; n < 4; ++n) {
            if (n == m)
                continue;
            second = nums[n];
            for (int o = 0; o < 4; ++o) {
                if (o == m || o == n)
                    continue;
                third = nums[o];
                for (int p = 0; p < 4; ++p) {
                    if (p == m || p == n || p == o)
                        continue;
                    fourth = nums[p];
                    for (int x = 0; x < 4; ++x) {
                        op1 = ops[x];
                        for (int y = 0; y < 4; ++y) {
                            op2 = ops[y];
                            for (int z = 0; z < 4; ++z) {
                                op3 = ops[z];
                                double cur_reslt = get_value({ first,second,third,fourth }, { op1,op2,op3 });
                                if (abs(cur_reslt - 24) < 0.001) {
                                    cout << first << get_op(op1) << second << get_op(op2) << third << get_op(op3) << fourth << endl;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    cout << "NONE" << endl;
}

void get_brother() {    //hj27
    REDIRECT_CIN
    int n;
    cin >> n;
    vector<string> str_arr(n);
    vector<map<char, int>> map_arr(n);
    for (int i = 0; i < n; ++i) {
        string temp;
        cin >> temp;
        str_arr.push_back(temp);
        map<char, int> cur_map{};
        for (auto& c : temp) {
            if (!cur_map.count(c))
                cur_map[c] = 1;
            else ++cur_map[c];
        }
        map_arr.push_back(cur_map);
    }
    string key_str;
    cin >> key_str;
    map<char, int> key_map{};
    for (auto& c : key_str) {
        if (!key_map.count(c))
            key_map[c] = 1;
        else ++key_map[c];
    }
    vector<string> brothers{};
    for (int i = 0; i < str_arr.size(); ++i) {
        auto& temp = str_arr[i];
        auto& m = map_arr[i];
        if (key_str.size() != temp.size() || key_str == temp)
            continue;
        if (m == key_map)
            brothers.push_back(temp);
    }
    int bro_index;
    cin >> bro_index;
    cout << brothers.size() << endl;
    if (brothers.size() != 0 && bro_index <= brothers.size())
        cout << brothers[bro_index - 1];
    cout << endl;
}

bool isPrime(int n) {
    if (n == 1) return true;
    for (int i = 2; i < n / 2 + 1; ++i) {
        if (n % i == 0)
            return false;
    }
    return true;
}

int get_couples(vector<int> nums) {
    int len = nums.size();
    if (len <2 ) return 0;
    int max_len = 0;
    for (int i = 0; i < len; ++i) {
        int first = nums[i];
        for (int j = 0; j < len; ++j) {
            if (j == i) continue;
            int second = nums[j];
            int sum = first + second;
            if (isPrime(sum)) {
                vector<int> new_vec{};
                for (int k = 0; k < len; ++k) {
                    if (k == i || k == j) continue;
                    new_vec.push_back(nums[k]);
                }
                int cur_len=1 + get_couples(new_vec);
                max_len = max(max_len, cur_len);
            } 
        }
    }
    return max_len;
}

void get_prime_couples() {  //hj28
    int n;
    cin >> n;
    vector<int> nums{};
    for (int i = 0; i < n; ++i) {
        int temp;
        cin >> temp;
        nums.push_back(temp);
    }
    cout << get_couples(nums) << endl;
}

void reverse_sentence() {   //jh31
    string line;
    getline(cin, line);
    vector<string> words{};
    auto it = line.cbegin();
    while (it != line.cend()) {
        while (it != line.cend()&&!((*it) >= 'a' && (*it) <= 'z' || (*it) >= 'A' && (*it) <= 'Z')) {
            ++it;
        }
        auto word_begin = it;
        while (it != line.cend()&&((*it) >= 'a' && (*it) <= 'z' || (*it) >= 'A' && (*it) <= 'Z')) {
            ++it;
        }
        auto word_end = it;
        words.push_back(string(word_begin, word_end));
    }
    for (auto crit = words.crbegin(); crit != words.crend()-1; ++crit)
        cout << *crit<<" ";
    cout << *(words.crend() - 1) << endl;
}

bool can_solve(vector<vector<int>> &plate) {
    bool found_start = false;
    bool finished = true;
    unordered_set<int> min_us{ 1,2,3,4,5,6,7,8,9 };
    pair<int, int> min_pos{};
    for (int i = 0; i < 9; ++i) {
        for (int j = 0; j < 9; ++j) {
            if (plate[i][j] == 0) {
                finished = false;
                unordered_set<int> us{ 1,2,3,4,5,6,7,8,9 };
                auto& row = plate[i];
                for (auto& n : row) {
                    auto it = us.find(n);
                    if (it != us.cend())
                        us.erase(it);
                }
                for (int k = 0; k < 9; ++k) {
                    auto it = us.find(plate[k][j]);
                    if (it != us.cend())
                        us.erase(it);
                }
                if (us.size() == 0)
                    return false;
                if (us.size() == 1) {
                    plate[i][j] = *us.cbegin();
                    found_start = true;
                    return can_solve(plate);
                }
                else {
                    if (min_us.size() > us.size()) {
                        min_us = us;
                        min_pos = {i,j};
                    }   
                }
            }
        }
    }
    if (finished)
        return true;
    if (!found_start) {
        auto mit = min_us.cbegin();
        while (mit != min_us.cend()) {
            vector<vector<int>> new_vec = plate; 
            int x = min_pos.first, y = min_pos.second;
            new_vec[x][y] = *mit;
            if (can_solve(new_vec)) {
                plate = new_vec;
                return true;
            }
            ++mit;
        }
        return false;
    }
        
}

void print_plate(vector<vector<int>> plate) {
    int m = plate.size(),n=plate[0].size();
    for (int i = 0; i < m; ++i) {
        for (int j = 0; j < n; ++j) {
            cout << plate[i][j]<<" ";
        }
        cout << endl;
    }
}

void sudoka() { //hj44
    //标准输入重定向到文件
    REDIRECT_CIN
    vector<vector<int>> plate(9, vector<int>(9));
    for (int i = 0; i < 9; ++i) {
        for (int j = 0; j < 9; ++j)
            cin >> plate[i][j];
    }
    if (can_solve(plate))
        print_plate(plate);
}

int _gcd(const int& a, const int& b) {
    if (a == 0) return b;
    if (a < b) return _gcd(b,a);
    return _gcd(a % b, b);
}

void get_real_numerator_number() {
    REDIRECT_CIN
    string num;
    cin >> num;
    vector<int> fracted_denominators{};
    bool unfinished = true;
    int index = num.find('/');
    int numerator = atoi(num.substr(0, index).c_str());
    int denominator = atoi(num.substr(index + 1).c_str());
    while (unfinished) {
        for (int i = 2; i <= denominator; ++i) {
            int gcd = _gcd(denominator, i);
            int i_mul = denominator / gcd, d_mul = i / gcd;
            int next_numerator = d_mul * numerator - i_mul;
            if (next_numerator == 0) {
                fracted_denominators.push_back(i);
                unfinished = false;
                break;
            }
            if (next_numerator > 0) {
                fracted_denominators.push_back(i);
                numerator = next_numerator;
                denominator *= d_mul;
                break;
            }
        }
    }
    for (int i = 0; i < fracted_denominators.size() - 1; ++i) {
        cout << "1/" << fracted_denominators[i] << "+";
    }
    cout << "1/" << fracted_denominators[fracted_denominators.size() - 1]<<endl;
}

void verify_pwd() {
    REDIRECT_CIN
    string pwd;
    while (cin >> pwd) {
        unordered_set<char> c_set{};
        bool has_A = false, has_a = false, has_9 = false, has_$= false;
        bool has_dup_sub = false;
        for (int i = 0; i < pwd.size();++i) {
            const char& c = pwd[i];
            if (c >= 'A' && c <= 'Z')
                has_A = true;
            else if (c >= 'a' && c <= 'z')
                has_a = true;
            else if (c >= '0' && c <= '9')
                has_9 = true;
            else
                has_$ = true;
            if (c_set.count(c)) {
                int index = pwd.find_first_of(c);
                int k = 1;
                for (; k < 3 && index + k < i && k + i < pwd.size(); ++k) {
                    if (pwd[index + k] != pwd[i + k])
                        break;
                }
                if (k > 2) {
                    has_dup_sub = true;
                    break;
                }
            }
            else
                c_set.insert(c);
        }
        if (has_A + has_a + has_9 + has_$ > 2 && !has_dup_sub)
            cout << "OK" << endl;
        else
            cout << "NG" << endl;
    }
}

void bag() {
    REDIRECT_CIN
    struct Item {
        int id;
        int price;
        int importance;
        int belong=-1;
        vector<int> subItems;
        vector<pair<int, int>> combinations;
    };
    int max_price,n;
    cin >>max_price>> n;
    Item items[5];
    for (int i = 0; i < n; ++i) {
        items[i].id = i;
        cin >> items[i].price>>items[i].importance>>items[i].belong;
        --items[i].belong;
    }
    for (auto& item : items) {
        if (item.belong == -1)
            continue;
        else
            items[item.belong].subItems.push_back(item.id);
    }
    vector<Item> mainItems{};
    for (auto& item : items) {
        if (item.belong == -1)
            mainItems.push_back(item);
    }
    for (auto& item : mainItems) {
        item.combinations.push_back({item.price,item.importance});
        for (int i = 0; i < item.subItems.size(); ++i) {
            auto temp = item.subItems[i];
            item.combinations.push_back({item.price+items[temp].price,item.importance+items[temp].importance});
        }
        if (item.subItems.size() > 1) {
            int price = item.price + items[item.subItems[0]].price + items[item.subItems[1]].price;
            int importance=item.importance+ items[item.subItems[0]].importance + items[item.subItems[1]].importance;
            item.combinations.push_back({price,importance});
        }  
    }
    vector<vector<int>> dp(max_price+1, vector<int>(mainItems.size()+1));
    for (int w = 10; w <= max_price; w += 10) {
        for (int i = 1; i <= mainItems.size(); ++i) {
            for (auto& c : mainItems[i-1].combinations) {
                if (w < c.first) continue;
                dp[w][i] = max(dp[w][i-1],dp[w-c.first][i-1]+c.first*c.second);
            }
        }
    }
    cout << dp[max_price][mainItems.size()] << endl;
}

void fill_apples() {
    REDIRECT_CIN
    int m, n;
    cin >> m >> n;
    vector<vector<int>> dp(m+1,vector<int>(n+1));
    for (int i = 0; i <= n; ++i)
        dp[0][i] = 1;
    for (int i = 0; i <= m; ++i)
        dp[i][0] = 1;
    for (int i = 1; i <= n; ++i)
        dp[1][i] = 1;
    for (int i = 1; i <= m; ++i)
        dp[i][1] = 1;
    if (m <= n) n = m;
    for (int i = 2; i <= m; ++i) {
        for (int j = 2; j <= n; ++j) {
            dp[i][j] = dp[i][j - 1] + (i >= j ? dp[i - j][j] : 0);
        }
    }
    cout << dp[m][n]<<endl;
}

bool contains(string& to_seek, string& target) {
    if (target.size() > to_seek.size()) return false;
    int index = to_seek.find(target[0]);
    for (int i = 0; i < target.size(); ++i) {
        if (i + index > to_seek.size() - 1 || to_seek[i + index] != target[i])
            return false;
    }
    return true;
}

void analyze_data() {
    REDIRECT_CIN
    int I, R;
    cin >> I;
    vector<int> arrI(I);
    for (int i = 0; i < I; ++i)
        cin >> arrI[i];
    cin >> R;
    vector<int> arrR(R);
    for (int i = 0; i < R; ++i)
        cin >> arrR[i];
    sort(arrR.begin(), arrR.end());
    auto it = unique(arrR.begin(), arrR.end());
    arrR.erase(it,arrR.end());
    vector<string> sarrI{};
    for (auto& n : arrI) {
        sarrI.push_back(to_string(n));
    }
    vector<int> result{};
    
    for (auto& n : arrR) {
        vector<int> cur_set{};
        string sn = to_string(n);
        for (int i = 0; i < arrI.size(); ++i) {
            if (contains(sarrI[i], sn)) {
                cur_set.push_back(i);
                cur_set.push_back(arrI[i]);
            }
        }
        int num = cur_set.size() / 2;
        if (num == 0) continue;
        result.push_back(n);
        result.push_back(num);
        result.insert(result.end(), cur_set.cbegin(), cur_set.cend());
    }
    int n = result.size();
    result.insert(result.begin(), n);
    for (int i = 0; i < result.size()-1; ++i)
        cout << result[i]<<" ";
    cout << result[result.size() - 1] << endl;
}



void encrypt(string &s) {
    string result;
    char ch;
    for (auto& c : s) {
        if (c >= 'a' && c < 'z')
            ch = toupper(c + 1);
        else if (c == 'z')
            ch = 'A';
        else if (c >= 'A' && c < 'Z')
            ch = tolower(c + 1);
        else if (c == 'Z')
            ch = 'a';
        else if (c >= '0' && c < '9')
            ch = c + 1;
        else if (c == '9')
            ch = '0';
        else ch = c;
        result.push_back(ch);
    }
    cout << result << endl;
}

void decrypt(string &s) {
    string result;
    char ch;
    for (auto& c : s) {
        if (c > 'a' && c <= 'z')
            ch = toupper(c -1);
        else if (c == 'a')
            ch = 'Z';
        else if (c > 'A' && c <= 'Z')
            ch = tolower(c - 1);
        else if (c == 'A')
            ch = 'z';
        else if (c > '0' && c <= '9')
            ch = c - 1;
        else if (c == '0')
            ch = '9';
        else ch = c;
        result.push_back(ch);
    }
    cout << result << endl;
}

vector<vector<int>> all_set{};
void subnet(int n,vector<vector<int>> last_set) {
    vector<vector<int>> cur_set{};
    if (last_set.size() == 0) {
        for (int i = 1; i <= n; ++i)
            cur_set.push_back({ i });
    }
    else if (last_set[0].size() == n)
        return;
    else {
        int len = last_set[0].size();
        for (auto &vec : last_set) {
            int last_element = vec[len - 1];
            for (int i = last_element + 1; i <= n; ++i) {
                vector<int> temp(vec);
                temp.push_back(i);
                cur_set.push_back(temp);
            }
        }
    }
    all_set.insert(all_set.end(), cur_set.cbegin(), cur_set.cend());
    subnet(n, cur_set);
}

struct TreeNode {
    int val;
    TreeNode* left;
    TreeNode* right;
    TreeNode(int x):val(x),left(nullptr),right(nullptr){}
};

TreeNode* buildTreeFromVec(vector<int> &vec,int n) {
    int len = vec.size();
    if (n >= len||vec[n]==-1) return nullptr;
    TreeNode* root = new TreeNode(vec[n]);
    root->left = buildTreeFromVec(vec, 2 * n + 1);
    root->right = buildTreeFromVec(vec, 2 * n + 2);
    return root;
}

TreeNode* mergeTrees(TreeNode* tree1, TreeNode* tree2) {
    if (tree1 == nullptr && tree2 == nullptr) return nullptr;
    TreeNode* root;
    if (tree1 == nullptr)
        root = tree2;
    else if (tree2 == nullptr)
        root = tree1;
    else {
        int val = 0;
        val += tree1->val;
        val += tree2->val;
        root = new TreeNode(val);
        root->left = mergeTrees(tree1->left, tree2->left);
        root->right = mergeTrees(tree1->right, tree2->right);
    }
    return root;
}

int main() {
    vector<int> vec_tree1{1,3,2,5};
    auto tree1 = buildTreeFromVec(vec_tree1, 0);
    vector<int> vec_tree2{ 2,1,3,-1,4,-1,7 };
    auto tree2 = buildTreeFromVec(vec_tree2, 0);
    auto root=mergeTrees(tree1, tree2);
    return 0;
}